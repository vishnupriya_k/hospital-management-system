import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';       
import { EnrollPatientComponent } from './enroll-patient/enroll-patient.component';
import { EnrollPhysicianComponent } from './enroll-physician/enroll-physician.component';
import { ViewPatientComponent } from './view-patient/view-patient.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { DiagnosisDetailsComponent } from './diagnosis-details/diagnosis-details.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { ItemsComponent } from './items/items.component';
import { NgxPaginationModule } from 'ngx-pagination';
        
               
@NgModule({ 
  declarations: [ 
    AppComponent,
    EnrollPatientComponent,
    EnrollPhysicianComponent,
    ViewPatientComponent,
    SearchPhysicianComponent,
    DiagnosisDetailsComponent,
    AdminComponent,       
    LoginComponent,  
    ItemsComponent
  ],
  imports: [  
    BrowserModule, 
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ], 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
