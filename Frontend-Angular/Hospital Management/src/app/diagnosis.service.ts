import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiagnosisService {

  private baseUrl = 'http://localhost:8080/api/diagnosis';

  constructor(private http: HttpClient) { }

  getPhysicianList():Observable<any>{
    return this.http.get(this.baseUrl);
  }

  
  
  
  generateDiagnosisReport(diagnosis:any):Observable<any>{
    return this.http.post(this.baseUrl,diagnosis);
  }
}
