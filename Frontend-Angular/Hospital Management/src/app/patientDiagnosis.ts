import { Input } from '@angular/core';
import { Patient } from './Patient';

export class PatientDiagnosis
{
    id:number;
    patientId:number;
    patient:Patient;
    symptoms:String;
    diagnosisProvided:String;
    administeredBy:String;
    dateOfDiagnosis:Date;
    followUpRequired:String;
    dateOfFollowUp:Date;
    billAmount:number;
    cardNumber:number;
    modeOfPayment:String;
}
