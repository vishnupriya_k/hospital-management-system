import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}) 
export class AppComponent {
  title = 'miniproject';
  log=false;
  home: boolean=false;
  constructor(private route:Router,public authguardService:AuthGuardService)
  {  
    this.route.navigate(['/item'])
    console.log(this.authguardService.isLoggedIn)
    this.log=this.authguardService.isLoggedIn;
  }
  ngOnInit() 
  { 
    this.home=true;
    console.log("On init")
  }
}
               