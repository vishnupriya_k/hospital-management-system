  import { Component, OnInit } from '@angular/core';
  import { PatientDiagnosis } from '../patientDiagnosis';
  import { DiagnosisService } from '../diagnosis.service';
  import { PatientService } from '../patient.service';
  import { PhysicianService } from '../physician.service';
  import { Observable } from 'rxjs';
  import { Physician } from '../Physician';    
  import { Patient } from '../Patient';     
  import { Router } from '@angular/router'; 
          
  @Component({            
    selector: 'app-diagnosis-details',
    templateUrl: './diagnosis-details.component.html',
    styleUrls: ['./diagnosis-details.component.css']
  }) 
  export class DiagnosisDetailsComponent implements OnInit {
    diag: PatientDiagnosis;
      
    constructor(private diagnosisService:DiagnosisService,private patientService:PatientService,private physicianService:PhysicianService,
  
      private route:Router) { }
    d:number;
    space:String=" ";
    dummy:Observable<Patient>;
    ngOnInit(): void {
      this.reloadData();
    }
    diagnosis:PatientDiagnosis=new PatientDiagnosis(); 
    submitted=false;

    newReport():void{
      this.submitted=false;
      this.diagnosis=new PatientDiagnosis();
    }     
    save(){ 
      console.log(this.diagnosis);   
      console.log(this.diagnosis.patientId);  
      this.diagnosisService.generateDiagnosisReport(this.diagnosis).
      subscribe(     
        data=>{   
          console.log(data);   
          
          this.submitted=true;
        },
        error=>console.log(error));
        this.diagnosis=new PatientDiagnosis();
    }
    onSubmit(){  
      this.diagnosis.billAmount=10000;
      this.diag=this.diagnosis;
      console.log(this.diag);
      this.dummy=this.patientService.getPatientbyId(this.d);
      
      console.log(this.dummy);
      this.save(); 
    } 
  
    patients:Observable<Patient[]>;
    physicians:Observable<Physician[]>;
    reloadData(){
      this.patients=this.patientService.getPatientList();
      this.physicians=this.diagnosisService.getPhysicianList();
    } 

    back()
    {
      this.route.navigateByUrl("item");
    }


  }
    