import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Observable } from 'rxjs';
import { Patient } from '../Patient';
import { Router } from '@angular/router';
  
@Component({ 
  selector: 'app-view-patient',
  templateUrl: './view-patient.component.html',
  styleUrls: ['./view-patient.component.css']
})
export class ViewPatientComponent implements OnInit {
  
  patients:Observable<Patient[]>; 
  p: number = 1;
  count: number = 5;

  constructor(private patientService:PatientService,private route:Router) { }

  ngOnInit() {
    this.reloadData();
  }  
  reloadData(){
    this.patients=this.patientService.getPatientList();
  }  
  

  //Pagination


 
  back()
  {
    this.route.navigateByUrl("item");
  }

}
 