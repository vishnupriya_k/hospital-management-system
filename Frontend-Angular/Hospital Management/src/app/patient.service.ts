import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  private baseUrl = 'http://localhost:8080/api/patient';

  constructor(private http: HttpClient) { }

  enrollPatient(patient: any): Observable<any>{
    console.log("enroll"+patient.dob+"----"+patient.state)
    return this.http.post(this.baseUrl,patient);
  }
  getPatientList():Observable<any>{
    console.log("vishnu priya kasula");
    return this.http.get(this.baseUrl);
  }
  getPatientbyId(id: number): Observable<any> {
    console.log("Service--"+id);
    return this.http.get(`${this.baseUrl}/id/${id}`);
  }
}
