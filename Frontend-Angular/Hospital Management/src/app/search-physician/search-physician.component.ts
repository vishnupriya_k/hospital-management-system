import { Component, OnInit } from '@angular/core';
import { Physician } from '../Physician';
import { PhysicianService } from '../physician.service';
import { Departments } from '../Departments';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-physician',
  templateUrl: './search-physician.component.html',
  styleUrls: ['./search-physician.component.css']
})
export class SearchPhysicianComponent implements OnInit {

  Departments:String[];

  selectedDepartment:String;


  States:String[];
 
  selectedState:String;


  physicians:Physician[];

  criteria:String;

  hideTable:boolean=true;

  constructor(private physicianService:PhysicianService,private dataService:DataService,private route:Router) { }
 
  ngOnInit() { 
  this.Departments=this.dataService.getDepartments();
  this.States=this.dataService.getStates();
  
  console.log(this.Departments)
  }
  private searchByDepartment() {
    this.physicians = [];
    this.physicianService.getPhysicianbyDepartment(this.selectedDepartment)
      .subscribe(physicians => this.physicians = physicians);
     // this.physicians=this.physicianService.getPhysiciansList();
  } 

  private searchByState() {
    this.physicians = [];
    this.physicianService.getPhysicianbyState(this.selectedState)
      .subscribe(physicians => this.physicians = physicians);
     // this.physicians=this.physicianService.getPhysiciansList();
  }
   
  onDepartmentChange() {
    
    this.selectedState="Select State";
    this.searchByDepartment();
    this.criteria="department";
    this.hideTable=false;
  }
  onStateChange() {
   
    this.selectedDepartment="Select Department";
    this.searchByState();
    this.criteria="state";
    this.hideTable=false;
  }

  
  
  //pagination
  
  p: number = 1;
  count: number = 5;   

  back()
  {
    this.route.navigateByUrl("item");
  }







}  
