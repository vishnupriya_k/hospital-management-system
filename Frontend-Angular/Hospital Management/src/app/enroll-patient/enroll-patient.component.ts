import { Component, OnInit } from '@angular/core';
import { Patient } from '../Patient';
import { PatientService } from '../patient.service';
import { State } from '../State';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-enroll-patient',
  templateUrl: './enroll-patient.component.html',
  styleUrls: ['./enroll-patient.component.css']
}) 
export class EnrollPatientComponent implements OnInit {

  patient: Patient = new Patient();
     

  submitted = false; 
   
  constructor( private patientService: PatientService,private dataService:DataService,private route:Router) { }

  states:String[];
    
  p: number = 1;
  count: number = 5;

  ngOnInit(): void {
    this.states=this.dataService.getStates();
    this.reloadData();
  }
 
  newPatient(): void{
    this.submitted = false;
    this.patient = new Patient();
  }
    
  save(){
    console.log("save"+this.patient.dob+"----"+this.patient.state)
    this.patientService.enrollPatient(this.patient)
    .subscribe(
      data=> {
        console.log(data);
        this.submitted = true;
      },    
      error => console.log(error));
      this.patient = new Patient();
  }
  
  onSubmit(){
    console.log("submit"+this.patient.insurancePlan+"----"+this.patient.state)
    this.save();
  } 

  patients:Observable<Patient[]>;

  reloadData(){
    this.patients=this.patientService.getPatientList();
  } 
  back()
  {
    this.route.navigateByUrl("item");
  }
 
  } 
 