export class Physician{
    id: number;
    firstName: string;
    lastName: string;
    department: string;
    educationalQualification: string;
    yearsOfExperience: string;
    state: string;
    insurancePlan: String;

}