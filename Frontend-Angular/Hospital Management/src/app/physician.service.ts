import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhysicianService {

  private baseUrl = 'http://localhost:8080/api/physician'

  constructor(private http: HttpClient) { }

  enrollPhysician(physician: any): Observable<any>{
    return this.http.post(this.baseUrl,physician);
  }
  getPhysicianbyDepartment(department: String): Observable<any> {
    console.log("Service"+department);
    return this.http.get(`${this.baseUrl}/department/${department}`);
  }

  getPhysicianbyState(state: String): Observable<any> {
    console.log("Service-state-"+state);
    return this.http.get(`${this.baseUrl}/state/${state}`);
  }
}
 