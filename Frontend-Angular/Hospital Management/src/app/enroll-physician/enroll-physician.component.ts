  import { Component, OnInit } from '@angular/core';
  import { Physician } from '../Physician';
  import { PhysicianService } from '../physician.service';
  import { DataService } from '../data.service';
  import { Observable } from 'rxjs';
  import { DiagnosisService } from '../diagnosis.service';
import { Router } from '@angular/router';

  @Component({
    selector: 'app-enroll-physician',
    templateUrl: './enroll-physician.component.html',
    styleUrls: ['./enroll-physician.component.css']
  })
  export class EnrollPhysicianComponent implements OnInit {

  
    physician: Physician = new Physician();
    submitted = false;
    
    states: String[];
    Departments: String[];

    constructor(private physicianService: PhysicianService,private diagnosisService:DiagnosisService,private dataService:DataService,private route:Router) { }

    ngOnInit(): void {
      this.states=this.dataService.getStates();
      this.Departments=this.dataService.getDepartments();
    //this.reloadData();
    }

    newPhysician(): void{
      this.submitted = false;
      this.physician = new Physician();
    }

    onSubmit(){ 
      this.save();
    }

    save(){
      this.physicianService.enrollPhysician(this.physician)
      .subscribe(
        data=> {
          console.log(data);
          this.submitted = true;
          this.reloadData();
        },
        error => console.log(error));
        this.physician = new Physician();
    }
  
    physicians:Observable<Physician[]>;

    reloadData(){
      this.physicians=this.diagnosisService.getPhysicianList();
      console.log(this.physicians);
    } 


        
    p: number = 1;
    count: number = 5;

    
    back()
    {
      this.route.navigateByUrl("item");
    }


  }
  