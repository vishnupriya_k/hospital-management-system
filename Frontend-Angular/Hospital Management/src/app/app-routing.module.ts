import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnrollPatientComponent } from './enroll-patient/enroll-patient.component';
import { EnrollPhysicianComponent } from './enroll-physician/enroll-physician.component';
import { ViewPatientComponent } from './view-patient/view-patient.component';
import { SearchPhysicianComponent } from './search-physician/search-physician.component';
import { DiagnosisDetailsComponent } from './diagnosis-details/diagnosis-details.component';
import { ItemsComponent } from './items/items.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuardService } from './auth-guard.service';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: 'enrollPatient', component: EnrollPatientComponent},
  {path: 'enrollPhysician', component: EnrollPhysicianComponent},
  {path: 'view', component: ViewPatientComponent},
  {path: 'search', component: SearchPhysicianComponent},
  {path: 'diagnosis', component: DiagnosisDetailsComponent},
 {
    path:'item',
    component:ItemsComponent
  }, 
  {
    path:'ItemsAdmin',
    component:AppComponent,
    canActivate:[AuthGuardService]
},
{
  path:'Login',
  component:LoginComponent
  },
  {
    path:'*',
    component:LoginComponent
    },

   
{
  path:'logout',
  component:AdminComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
   