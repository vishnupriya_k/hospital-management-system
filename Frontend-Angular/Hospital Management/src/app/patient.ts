export class Patient{
    patientId: Patient;
    firstName: string;
    lastName: string;
    password: string;
    dob: Date;
    email: string;
    contactNumber: number;
    state: string;
    insurancePlan: boolean;
}