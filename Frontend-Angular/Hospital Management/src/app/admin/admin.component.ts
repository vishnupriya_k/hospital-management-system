import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthserviceService } from '../authservice.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private route:Router,private _authserviceService:AuthserviceService) { }

  ngOnInit(): void {
    this.logout();
  }
  logout():void 
  { 
    console.log("logout");
    this._authserviceService.logOut();
    this.route.navigate(['/ItemsAdmin']);
  }

}
